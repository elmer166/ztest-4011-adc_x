/* \file 4011-ADC.c
 *
 * \brief ADC demo on dsPIC30F4011 "dsPIC-EL" board
 *
 *
 * Author: jjmcd
 *
 * Created on September 2, 2012, 10:03 AM
 */

#include <p30Fxxxx.h>
#include <stdio.h>
#include <stdlib.h>
#include "../4011-LCDlib-D.X/lcd.h"
#include "../4011-LCDlib-D.X/delay.h"

// Configuration fuses
_FOSC (XT_PLL16 & PRI)                  // 7.3728 rock * 16 = 118MHz
_FWDT (WDT_OFF)                         // Watchdog timer off
_FBORPOR (PWRT_16 & PBOR_OFF & MCLR_EN) // Brownout off, powerup 16ms
_FGS (GWRP_OFF & CODE_PROT_OFF)         // No code protection

//! Value from the A/D converter
unsigned int potValue;
//! Remember whether analog value has been read
unsigned int analogRead;
//! Previous value
unsigned int nOldValue;
//! Indicate whether to display text message
int nDirty;
//! Calculated voltage
float fVoltage;
//! Work string
char szWork[32];

//	Function Prototypes
int main (void);

//! main - Display the pot position

int main (void)
{
 
  // Make PORTD LED pins outputs
  TRISD &= 0xfff1;
  // Turn off red, yellow LEDs, green on
  LATD |= 0x000a;

  // Initialize ADC
  /* set port configuration here */
  ADPCFGbits.PCFG8 = 0; // ensure AN8/RB8 is analog
  /* set channel scanning here, auto sampling and convert,
     with default read-format mode */
  ADCON1 = 0x00E4;
  /* No channel scan for CH0+, Use MUX A,
     SMPI = 1 per interrupt, Vref = AVdd/AVss */
  ADCON2 = 0x0000;
  /* Set Samples and bit conversion time */
  ADCON3 = 0x1f3f; //(as slow as possible)
  /* set channel scanning here for AN8 */
  ADCSSLbits.CSSL8 = 1;
  /* channel select A3 */
  ADCHSbits.CH0SA3 = 1;
  /* reset ADC interrupt flag */
  IFS0bits.ADIF = 0;
  /* enable ADC interrupts */
  IEC0bits.ADIE = 1;
  /* turn on ADC module */
  ADCON1bits.ADON = 1;

  // Welcome message so we know something is happening
  LCDinit ();
  LCDclear ();
  Delay (Delay_1S_Cnt);
  LCDputs ("And away we go!");
  Delay (Delay_1S_Cnt);
  LATD |= 0x000e;                               // Green off

  // Set previous ADC result to something impossible
  nOldValue = 10000;

  while (1)
    {
      if (analogRead)                           // Conversion finished?
        {
          LATD &= 0xfffd;                       // LED on
          if (abs (potValue - nOldValue) > 10)   // Significant change?
            {
              nOldValue = potValue;             // Remember value
              // Calculate voltage
              fVoltage = 5.0 * (float) potValue / 1024.0;
              // Create string for display
              sprintf (szWork, "  %5d    %6.3f", potValue, (double)fVoltage);
              LCDposition (0x40);               // Result on line 2
              LCDputs (szWork);                 // Display
            }
          analogRead = 0;                       // Reset read flag
          LATD |= 0x0002;                       // Turn off LED
        }
    }
}

//! ADC Interrupt Service Routine

/*!
 * Whenever an analog value is available, thie routine will:
 * \li Clear the interrupt flag
 * \li Grab the analog value and store it in potValue
 * \li Increment analogRead
 *
 */
void __attribute__ ((__interrupt__, auto_psv))
_ADCInterrupt (void)
{
  IFS0bits.ADIF = 0; // Clear A/D interrupt flag
  potValue = ADCBUF0; // Save the potentiometer value
  analogRead++; // Remember it has been read
}
